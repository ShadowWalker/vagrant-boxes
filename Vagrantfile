require 'yaml'
vagrant_dir = File.expand_path(File.dirname(__FILE__))
vconfig = JSON.parse(File.read(vagrant_dir + "/settings.json"))

#
#First we'll do some checks on the configuration
#
if !vconfig['config']['php_install'] && vconfig['config']['mysql_pma_install']

    abort "You have to enable PHP installation inorder for phpMyAdmin to work!"
    
elsif !vconfig['config']['mysql_install'] && vconfig['config']['mysql_pma_install']

    abort "You have to enable MySQL Server installation inorder for phpMyAdmin to work!"
    
end

if vconfig['config']['php_install'] && !vconfig['config']['extras_install']
    abort "You have to enable the installation of the extras inorder for the PHP installation to work correctly."
end

#
#Let's do some magic
#
Vagrant.configure("2") do |config|
	config.vm.box = "hashicorp/precise64"
    config.omnibus.chef_version = '11'
    config.vm.box_download_insecure = true
    config.vm.hostname = vconfig['config']['hostname']
    config.vm.post_up_message = "Als er nog vragen of suggesties zijn, kan je die altijd doorspelen naar Sebastiaan (sebastiaan.provost@crosscheck.be)"
    
    config.vm.provider "virtualbox" do |v|
        v.memory = vconfig['config']['memory']
        v.cpus = vconfig['config']['cpus']
    end
    
    config.vm.provision "chef_solo" do |chef|
        chef.roles_path = "./chef_roles"
        chef.cookbooks_path = "./cookbooks"
        
        #
        #Enable debugging
        #
        if vconfig['debugging']
           chef.log_level = :debug
        end

        #
        #The following block contains is the starting line of code for all the necessary json data.
        #This JSON data will be passed on the recipe in order for chef to work without problems.
        #
        #

        chef.json.merge!(vconfig)
        
        #Adding recipe for the necessary apt functions (updates, upgrades etc...)
        #
        chef.add_recipe "apt"
                
        #
        #Adding the apache recipe if the config option for apache is set to y
        #
        if vconfig['config']['apache_install']
            chef.add_recipe "vagrant_1"
        end
        
        #
        #Adding the PHP recipe if the config option for php installation is set to y.
        #It also reads out the php version.
        #
        
        if vconfig['config']['php_install']
        #    chef.json.merge!(
        #        "php" => {
        #            "php_version" => vconfig['php_ver'],
        #            "pma_pass_root"    => vconfig['mysql_root_password'],
        #            "pma_install"   =>  vconfig['mysql_pma_install']
        #            }
        #        )
                
            chef.add_recipe "php_packages"
        end
        
        #
        #Adding the mysql recipe if the config option for apache is set to y
        #We also read out the mysql root password
        #
        if vconfig['config']['mysql_install']
            $server_root_password ||= vconfig['mysql_root_password']
            
            chef.json.merge!(
                "mysql" =>  {
                    "remove_anonymous_users" => true,
                    "remove_test_database" => true,
                    "allow_remote_root" => true,
                    "server_root_password" => $server_root_password,
                    "server_repl_password" => $server_root_password,
                    "server_debian_password" => $server_root_password
                            }
                        )
            chef.add_recipe "mysql::server"
        end
        
        if vconfig['config']['extras_install']
        
            chef.add_recipe "extras"
        end
        
        if vconfig['config']['ruby_install']
            chef.add_recipe "ruby_packages"
        end
        if vconfig['config']['nodejs_install']
            chef.add_recipe "nodejs_packages"
        end
        if vconfig['config']['drupal_install']
            chef.add_recipe "drupal_packages"
        end        

    end
    
    config.vm.network :private_network, ip: "192.168.3.10"
    config.hostsupdater.remove_on_suspend = true
    config.hostsupdater.aliases = []
    vconfig['config']['vhosts'].each do |i, vhost|
        if vhost['url'] != ''
            config.hostsupdater.aliases += ["#{vhost['url']}"]
        end
    end
    
    config.vm.network :forwarded_port, host: 4567, guest: 80
    config.vm.network :forwarded_port, host: 2223, guest: 22
    config.vm.network :forwarded_port, host: 3306, guest: 3306
    config.vm.network :forwarded_port, host: 9001, guest: 9001
end
