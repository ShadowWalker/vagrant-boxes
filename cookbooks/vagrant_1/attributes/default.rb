#
default['mysql']['service_name'] = 'default'

# passwords
default['mysql']['server_root_password'] = 'crosslang'
default['mysql']['server_debian_password'] = nil
default['mysql']['server_repl_password'] = nil

# used in grants.sql
default['mysql']['allow_remote_root'] = false
default['mysql']['remove_anonymous_users'] = true
default['mysql']['root_network_acl'] = nil

# port
default['mysql']['port'] = '3306'
