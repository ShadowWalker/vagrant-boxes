#
# Cookbook Name:: nodejs_packages
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package 'python-software-properties' do
    action :install
end

apt_repository "nodejs" do
  uri "http://ppa.launchpad.net/chris-lea/node.js/ubuntu"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "C7917B12"
end

package 'nodejs' do
    action :install
end

#package 'npm' do
#    action :install
#end

bash 'install_grunt_cli' do
    code <<-EOF
    npm install -g grunt-cli
    EOF
end