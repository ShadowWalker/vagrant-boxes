#
# Cookbook Name:: vagrant_1
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
print "Installing Apache2 MPM Worker and FastCGI Mod"

package 'apache2-mpm-worker' do
    action :install
end

package 'libapache2-mod-fastcgi' do
    action :install
end

service 'apache2' do
    action [ :enable, :start ]
end

directory '/var/www' do
    recursive true
    action :delete
end

link '/var/www' do
    to '/vagrant'
end

node['config']['vhosts'].each do |vhost_name, vhost|
    bash "Adding_vhost" do
        code <<-EOF
            cd /etc/apache2/sites-available/
            touch /etc/apache2/sites-available/#{vhost['url']}
            echo '<VirtualHost *:80>' >> #{vhost['url']}
	        echo 'ServerAdmin #{vhost['email']}' >> #{vhost['url']}
	        echo 'DocumentRoot /var/www/#{vhost['document_root']}' >> #{vhost['url']}
            echo 'ServerName #{vhost['url']}' >> #{vhost['url']}
	        echo 'ErrorLog ${APACHE_LOG_DIR}/#{vhost['url']}.error.log' >> #{vhost['url']}
	        echo 'CustomLog ${APACHE_LOG_DIR}/#{vhost['url']}.access.log combined' >> #{vhost['url']}
            echo '</VirtualHost>'>> #{vhost['url']}
        EOF
    end

    link '/etc/apache2/sites-available/'+vhost['url'] do
        to '/etc/apache2/sites-enabled/'+vhost['url']
    end

    directory '/vagrant/'+vhost['url'] do
        owner "www-data"
        group "www-data"
        mode 00755
        action :create
    end
end





