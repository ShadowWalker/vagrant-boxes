#
# Cookbook Name:: ruby_packages
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

print "Installing Ruby RVM and Ruby Bundler"

bash "install_correct_rvm" do
    code <<-EOF
    curl -L https://get.rvm.io | bash -s stable --ruby --autolibs=enable --auto-dotfiles
    EOF
end

package 'ruby-bundler' do
    action :install
end

package 'ruby-sass' do
    action :install
end

package 'ruby-compass' do
    action :install
end
