#
# Cookbook Name:: drupal_packages
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package 'drush' do
    action :install
end

package 'drush-make' do
    action :install
end