#
# Cookbook Name:: extras
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
package 'curl' do
    action :install
end

service 'php5-fpm' do
    action [:enable, :start]
end

puts "Let's do some cleaning up and enable some necessary mods for apache2!"

file "/etc/apache2/mods-enabled/fastcgi.conf" do
    action :delete
end

file "/etc/apache2/mods-enabled/fastcgi.load" do
    action :delete
end

file "/etc/apache2/mods-enabled/actions.conf" do
    action :delete
end

file "/etc/apache2/mods-enabled/actions.load" do
    action :delete
end

file "/etc/apache2/mods-enabled/alias.load" do
    action :delete
end

file "/etc/apache2/mods-enabled/alias.conf" do
    action :delete
end

template "/etc/apache2/mods-enabled/fastcgi.conf" do
    source "fastcgi.conf"
    mode "0644"
end

template "/etc/apache2/mods-enabled/fastcgi.load" do
    source "fastcgi.load"
    mode "0644"
end

template "/etc/apache2/mods-enabled/actions.conf" do
    source "actions.conf"
    mode "0644"
end

template "/etc/apache2/mods-enabled/actions.load" do
    source "actions.load"
    mode "0644"
end

template "/etc/apache2/mods-enabled/alias.conf" do
    source "alias.conf"
    mode "0644"
end

template "/etc/apache2/mods-enabled/alias.load" do
    source "alias.load"
    mode "0644"
end

print "Mods enabled"

file "/etc/apache2/conf.d/php-fpm.conf" do
    owner "root"
    group "root"
    mode "0755"
    action :create
end

template "/etc/apache2/conf.d/php-fpm.conf" do
  source "php-fpm.conf"
  mode "0644"
  notifies :restart, "service[apache2]", :delayed
end

file "/etc/php5/fpm/pool.d/www.conf" do
    action :delete
end

template "/etc/php5/fpm/pool.d/www.conf" do
    source "www.conf"
    mode "0644"
    notifies :restart, "service[php5-fpm]", :delayed
end




