#
# Cookbook Name:: php_packages
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

if node['config']['php_ver'] == "5.4"
    
    print "Adding extra repository for PHP 5.4"

    apt_repository "php54" do
        uri "http://ppa.launchpad.net/ondrej/php5-oldstable/ubuntu"
        distribution node['lsb']['codename']
        components ["main"]
        keyserver "keyserver.ubuntu.com"
        key "E5267A6C"
    end

    print "Installing PHP-FPM (5.4) and additional packages."
else
    print "Installing PHP-FPM(5.3) and additional packages."
end

package 'php5-fpm' do
    action :install
end 

package 'php5' do
    action :install
end

package 'php5-gd' do
    action :install
end

package 'php5-mysql' do
    action :install
end

if node['config']['mysql_pma_install']
    

    bash "extra_phpmyadmin_parameters" do
        code <<-EOF
            echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | debconf-set-selections
            echo 'phpmyadmin phpmyadmin/app-password-confirm password #{node['config']['mysql_root_password']}' | debconf-set-selections
            echo 'phpmyadmin phpmyadmin/mysql/admin-pass password #{node['config']['mysql_root_password']}' | debconf-set-selections
            echo 'phpmyadmin phpmyadmin/mysql/app-pass password #{node['config']['mysql_root_password']}' | debconf-set-selections
            echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections
        EOF
    end

    package 'phpmyadmin' do
        action :install
    end

    package 'php5-xdebug' do
        action :install
    end

    bash "append_to_xdebug_config" do
    #user "root"
        code <<-EOF
            echo "xdebug.remote_host = localhost" >> /etc/php5/fpm/conf.d/20-xdebug.ini
            echo "xdebug.remote_port = 9001" >> /etc/php5/fpm/conf.d/20-xdebug.ini
            echo "xdebug.remote_enable = 1" >> /etc/php5/fpm/conf.d/20-xdebug.ini
        EOF
        not_if "grep -q 9001 /etc/php5/fpm/conf.d/20-xdebug.ini"
    end
end